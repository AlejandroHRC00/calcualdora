@extends ('layout')

@section('content')
<p>
    <h1>Calculadora</h1>
    <form action="{{ route('resultado')}}" method="POST">
        @csrf
        <input type="number" name="n1" placeholder="Ingresa un numero" />
        <br>
        <input type="number" name="n2" placeholder="Ingresa otro numero" />
        <br>
        <select name="operador">
            <option value="Sumar">Sumar</option>
            <option value="Restar">Restar</option>
            <option value="Multiplicar">Multiplicar</option>
            <option value="Dividir">Dividir</option>
        </select>
        <br>
        <input type="submit" value="Calcular" />
    </form>
</p>
@endsection
