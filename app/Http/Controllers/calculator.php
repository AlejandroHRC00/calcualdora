<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class calculator extends Controller
{
    public function calcular(Request $request){
        $validated = $request ->validate([
            'n1' => 'required | numeric',
            'n2' => 'required | numeric',
            'operador' => 'required | in:Sumar,Restar,Multiplicar,Dividir'
        ]);

        $n1=$validated['n1'];
        $n2=$validated['n2'];
        $o=$validated['operador'];

        $r = '0';
        if($o == 'Sumar'){
            $r = $n1 + $n2;
        }else if($o == 'Restar'){
            $r = $n1 - $n2;
        }else if($o == 'Multiplicar'){
            $r = $n1 * $n2;
        }else{
            if($n2 == 0){
                return redirect()->back()->withErrors("No se puede dividr entre 0");
            }else{
                $r = $n1 / $n2;
            }
        }

        return view('resultado', ['resultado' => $r]);
    }
}
